'use strict';

/**
 * @ngdoc function
 * @name taskListAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the taskListAppApp
 */
angular.module('taskListAppApp')
  .controller('MainCtrl', function ($scope) {
    $scope.todoList = [{
		todoText: 'In case of Fire',
		done: false
	}, {
		todoText: 'git commit',
		done: false
	}, {
		todoText: 'git push',
		done: false
	}, {
		todoText: 'exit the building!',
		done: false
	}];


	$scope.getTotalTodos = function () {
		return $scope.todoList.length;
	};


	$scope.todoAdd = function () {
		// Checking for null or empty string
		if (null !== $scope.taskDesc && "" !== $scope.taskDesc) {
			$scope.todoList.push({
				todoText: $scope.taskDesc,
				done: false
			});
		}
    return $scope.todoList.length;
	};

	// Function to remove the list items
	$scope.remove = function () {
		var oldList = $scope.todoList;
		$scope.todoList = [];
		angular.forEach(oldList, function (x) {
			if (!x.done) {
				$scope.todoList.push(x);
			}
		});
    return $scope.todoList.length;
  };
});
