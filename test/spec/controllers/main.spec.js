"use strict"

describe('Controller: MainCtrl', function () {
var scope; //This declaration is important
	//describe your object type
	//load module

	beforeEach(angular.mock.module('taskListAppApp'));
	describe('MainCtrl', function () {
		//describe your app name


		var todoCtrl;
		beforeEach(inject(function ($controller, $rootScope) {
			scope = $rootScope.$new();
			todoCtrl = $controller('MainCtrl', {
				//What does this line do?
				$scope: scope
			});
		}));

		//Validatory tests
    it('trial test for toEqual', function(){
      var a = 4;
      expect(a).toEqual(4);
			// For debugging
			console.log(scope);
    });

		// DEFINED
		it('should have todoCtrl defined', function () {
			expect(todoCtrl).toBeDefined();
		});

		it('should have todoList defined', function() {
			expect(scope.todoList).toBeDefined();
		});

		it('should have 4 items by default', function() {
			var currentLength = scope.todoList.length;
			expect(currentLength).toBe(4);
		});

		it('should have add method defined', function(){
			expect(scope.todoAdd).toBeDefined();
		});

		it('should have remove method defined', function(){
			expect(scope.remove).toBeDefined();
		});

		it('should have getTotalTodos method defined', function(){
			expect(scope.getTotalTodos).toBeDefined();
		});


		// Method actions checked here
		it('getTotalTodos should return length', function() {
			expect(scope.getTotalTodos()).toBe(scope.todoList.length);
		});

		it('should increase the item count when todoAdd called', function() {
			// length should increase by one
			var currentLength = scope.todoList.length;
			expect(scope.todoAdd()).toBe(currentLength + 1);
		});

		it('should decrease the item count when remove called', function() {
			// length should decrease by one
			// But only if something is marked as done = text-truncate
			// Q: How to do that?
			// A: Setting 1 item to be done
			scope.todoList[0].done = true;
			var currentLength = scope.todoList.length;
			expect(scope.remove()).toBe(currentLength - 1);
		});

		// PENDING SPECS

	});
});
